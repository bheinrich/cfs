# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: indent-width 2; encoding utf-8; auto-brackets on;
# kate: mixedindent on; indent-mode cstyle; line-numbers on;
# kate: syntax cmake; replace-tabs on; background-color #D1EBFF;
# kate: remove-trailing-space on; bracket-highlight-color #ff00ff;

#=============================================================================
# Set global variables, configured from the external build.
#=============================================================================
set(ilupack_source  "@ilupack_source@")
set(CFS_SOURCE_DIR "@CFS_SOURCE_DIR@")
set(CMAKE_Fortran_COMPILER_ID "@CMAKE_Fortran_COMPILER_ID@")
set(CFS_DISTRO "@CFS_DISTRO@")
set(CFS_DEPS_CACHE_DIR "@CFS_DEPS_CACHE_DIR@")
set(CFS_KEY_ILUPACK "@CFS_KEY_ILUPACK@")
set(ILUPACK_ZIP "@ILUPACK_ZIP@")

#=============================================================================
# Include some convenient macros (e.g. for applying patches).
#=============================================================================
INCLUDE("${CFS_SOURCE_DIR}/cmake_modules/CFS_macros.cmake")

# unpack the encrypted file
execute_process(COMMAND unzip -o -P @CFS_KEY_ILUPACK@ @CFS_DEPS_CACHE_DIR@/sources/ilupack/@ILUPACK_ZIP@)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND} -E copy_if_different 
  "${CFS_SOURCE_DIR}/cfsdeps/ilupack/CMakeLists.txt"
  "${ilupack_source}/CMakeLists.txt"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND} -E copy_if_different 
  "${CFS_SOURCE_DIR}/cfsdeps/ilupack/ilupack_config.h.in"
  "${ilupack_source}/include/ilupack_config.h.in"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND} -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/ilupack/cc_etimes.c"
  "${ilupack_source}/tools/cc_etimes.c"
)

#=============================================================================
# Apply some patches.
#=============================================================================

SET(patches
  "ilupack-nomore-malloc-header.patch"
  "ilupack-sparspak-revrse.patch"
  "ilupack-disable-saddle-point-stuff.patch" )

if(CFS_DISTRO STREQUAL "MACOSX") 
  LIST(APPEND patches "ilupack-globals.patch" )
endif(CFS_DISTRO STREQUAL "MACOSX") 

LIST(APPEND patches
  # Use the standard amd_preprocess routine from Suitesparse and just provide
  # a wrapper ilupack_amd_preprocess inside ILUPACK
  "ilupack-amd-preprocess.patch"
  
  # Do not define USE_LAPACK_DRIVER in ilupack.h but rather from CMake.
  "ilupack-use-lapack-driver.patch"

  "ilupack-mainsym3.patch" )

APPLY_PATCHES("${patches}" "${CFS_SOURCE_DIR}/cfsdeps/ilupack")
