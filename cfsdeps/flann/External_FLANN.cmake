# FLANN - Fast Library for Approximate Nearest Neighbors
# https://github.com/flann-lib/flann

# make sure not to uninetendently use another packages settings. Supports assert_set() checks. Is mandatory!
clear_depencency_variables()

# set mandatory variables for the macros in DependencyTools.cmake.
set(PACKAGE_NAME "flann")
# 1.9.2 has the issues that is requires liblz4.a (cmake in build cmake) and that it failes on Windows "Could NOT find PkgConfig (missing: PKG_CONFIG_EXECUTABLE)"
set(PACKAGE_VER "1.9.1")
set(PACKAGE_FILE "${PACKAGE_VER}.zip")
set(PACKAGE_MD5 "4a6cc62db8ed09dd8a0c6537f6720f12")
set(DEPS_VER "") # set to "-a", "-b", when dependency changed with same PACKAGE_VER. Reset to "" with new PACKAGE_VER.

if(USE_OPENMP)
  set(DEPS_ID "OPENMP")
else()
  set(DEPS_ID "NO_OPENMP")
endif()

# the mirrors can point to arbitrary file names. 
set(PACKAGE_MIRRORS "https://github.com/flann-lib/flann/archive/refs/tags/${PACKAGE_FILE}")
# add default mirrors to PACKAGE_MIRRORS or replace all with LOCAL_PACKAGE_FILE if we already have it
add_standard_mirrors_or_set_local()

 # we only have a fortran compiler
use_c_and_fortran(ON OFF)

# sets PRECOMPILED_PCKG_FILE to the full precompiled name including path
set_precompiled_pckg_file()

set_package_library_list_lib_prefix("flann_cpp_s")

# set hidden cache variables *_LIBRARY = PACKAGE_LIBRARY, *_INCLUDE and some defaults
set_standard_variables()
# this is the standard target for cmake projects but we don't want to use install_manifest.txt but pick the stuff
set(DEPS_INSTALL "${DEPS_PREFIX}/install")

# set DEPS_ARG with defaults for a cmake project
set_deps_args_default() 
# add the specific settings for the packge which comes in cmake style
set(DEPS_ARGS
  ${DEPS_ARGS}
  -DBUILD_DOC:BOOL=OFF
  -DBUILD_EXAMPLES:BOOL=OFF
  -DBUILD_C_BINDINGS:BOOL=OFF
  -DBUILD_MATLAB_BINDINGS:BOOL=OFF
  -DBUILD_PYTHON_BINDINGS:BOOL=OFF
  -DBUILD_TESTS:BOOL=OFF
  -DUSE_MPI:BOOL=OFF
  -DUSE_OPENMP:BOOL=${USE_OPENMP})

# copy "static" license as we configure this dependency. Check if license is still valid!
file(COPY "${CMAKE_SOURCE_DIR}/cfsdeps/${PACKAGE_NAME}/license/" DESTINATION "${CMAKE_BINARY_DIR}/license/${PACKAGE_NAME}" )

# Generate ${PACKAGE_NAME}-patch.cmake we use for our external project
generate_patches_script() # sets PATCHES_SCRIPT

# generate package ceation script. We get the files from an install directory
generate_packing_script_install_dir()

# we have no postinstall, so don't call generate_postinstall_script()
assert_unset(POSTINSTALL_SCRIPT)

#dump_depencency_variables()

# do we want to use precompiled and do we already have the package?
if(${CFS_DEPS_PRECOMPILED} AND EXISTS "${PRECOMPILED_PCKG_FILE}")
  # copy files from cache
  create_external_unpack_precompiled()

# if not, build newly and possibly pack the stuff
else()
  create_external_cmake_patched()  

  # new data just built: shall we pack and store as precompiled?
  if(${CFS_DEPS_PRECOMPILED})
    # add custom step to zip a precompiled package to the cache.
    add_external_storage_step()
  endif()  
endif()

# add project to global list of CFSDEPS
set(CFSDEPS ${CFSDEPS} ${PACKAGE_NAME})