
SET(PARDISO_SRCS PardisoSolver.cc PardisoSolverPrimitive.cc)

ADD_LIBRARY(pardiso-olas STATIC ${PARDISO_SRCS})

# CFS_FORTRAN_LIBS has been set in cmake_modules/distro.cmake
# LAPACK_LIBRARY and BLAS_LIBRARY are defined in
# cmake_modules/FindFortranLibs.cmake
SET(TARGET_LL
  ${PARDISO_LIBRARY}
  ${LAPACK_LIBRARY}
  ${BLAS_LIBRARY}
  ${CFS_FORTRAN_LIBS})

TARGET_LINK_LIBRARIES(pardiso-olas ${TARGET_LL})

ADD_DEPENDENCIES(pardiso-olas cfsdeps)
