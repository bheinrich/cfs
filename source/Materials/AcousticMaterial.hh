// -*- mode: c++; coding: utf-8; indent-tabs-mode: nil; -*-
// kate: space-indent on; indent-width 2; encoding utf-8;
// kate: auto-brackets on; mixedindent off; indent-mode cstyle;

#ifndef ACOUSTICMATERIAL_DATA
#define ACOUSTICMATERIAL_DATA

#include "General/defs.hh"
#include "General/Environment.hh"
#include "BaseMaterial.hh"

namespace CoupledField {

  //! Class for Material Data
  /*! 
     Class for handling acoustic material data
  */

  class AcousticMaterial : public BaseMaterial {

  public:

    //! Default constructor
    AcousticMaterial(MathParser* mp,
                     CoordSystem * defaultCoosy);

    //! Destructor
    ~AcousticMaterial();

    //! Trigger finalization of material
    virtual void Finalize();
  };

} // end of namespace

#endif
